/*
Claire Sonderman, csonderm
Operating Systems Project 1
February 2, 2016

This program takes a file in one location and makes a copy.
If the target file exists, the program overwrites it.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <openssl/md5.h>

#define MAXDATASIZE 2048

// Alarm message, display after 1 second repeatedly
void display_message( int s )
{
	printf("copyit: still copying...\n");
	alarm(1);	
}

int main(int argc, char *argv[])
{
	// Set up variables
	int bytes_read = 0;		// Bytes read to sourcefile
	int bytes_written = 0;		// Bytes written to targetfile
	int bytes = 0;			// Total bytes for targetfile
	int fd1, fd2;			// File descriptor 1 = source, file descriptor 2 = target
	char* sourcefile = argv[1];	
	char* targetfile = argv[2];
	char buf[MAXDATASIZE];		// Buffer for reading and writing to files
	int close1;			// Return for closing source file
	int close2;			// Return for opening source file



	// Signal for still copying message, set alarm
	if ( signal(SIGALRM,display_message) == SIG_ERR ) {
		printf("Could not create signal: %s\n", strerror(errno));
		exit(1);
	}
	alarm(1);



	// Check for proper invocation, exit if wrong
	if ( argc < 3 ) {
		printf("copyit: Too few arguments!\n");
		printf("usage: copyit <sourcefile> <targetfile>\n");
		exit(1);
	}
	if ( argc > 3 ) {
		printf("copyit: Too many arguments!\n");
		printf("usage: copyit <sourcefile> <targetfile>\n");
		exit(1);
	}



	// Open source file
	fd1 = open(sourcefile,O_RDONLY,0);
	if ( fd1 < 0 ) {
		printf("Unable to open %s: %s\n", sourcefile, strerror(errno));
		exit(1);
	}



	// Create target file
	fd2 = creat(targetfile, 0600);
	if ( fd2 < 0 ) {
		printf("Unable to create %s: %s\n", targetfile, strerror(errno));
		exit(1);
	}


	// Loop to read data from source file and write to target file
	while (1) {
		bytes_read = read(fd1, buf, MAXDATASIZE);
		if ( bytes_read < 0) {
			if ( errno == EINTR ) {
				while( errno == EINTR ){
					bytes_read = read(fd1, buf, MAXDATASIZE);
				}
			}
			else {
				printf("Unable to read %s: %s\n", sourcefile, strerror(errno));	
				exit(1);
			}
		}
		if ( bytes_read == 0 ) {
			break;
		}
		bytes_written = write(fd2, buf, bytes_read);
		if ( bytes_written < bytes_read ) {
			if ( errno == EINTR ) {
				while( errno == EINTR ){
					bytes_written = write(fd2, buf, MAXDATASIZE);
				}
			}
			else {
				printf("Unable to write to %s: %s\n", targetfile, strerror(errno));	
				exit(1);
			}
		}
		if ( bytes_read == 0 ) {
			break;
		}
		bytes+=bytes_written;
	}



	// Close both files
	close1 = close(fd1);
	if ( close1 < 0 ) {
		printf("Unable to close %s: %s\n", sourcefile, strerror(errno));
		exit(1);
	}

	close2 = close(fd2);
	if ( close2 < 0 ) {
		printf("Unable to close %s: %s\n", targetfile, strerror(errno));
		exit(1);
	}


	// Print success message
	printf("copyit: Copied %i bytes from file %s to %s.\n", bytes, sourcefile, targetfile);

	return 0;
}


