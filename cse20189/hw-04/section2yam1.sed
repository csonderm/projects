
s/\\label//
/^\s*$/d
/blah/d
s/{//
s/}//
s/ {/ /
s/t}/t/
s/{m/m/
s/\ *\(tommy Traddles\|Mr. Micawber's gauntlet\|i visit Steetforth at his home, again\)/\U\1/

/\\\(sub\)*section/ {
  H
}

$ {
  G
}
s/\\section//
s/\\subsection/  /
s/\\subsubsection/    /
s/mylabel//
s/Some section titles/{Some section titles}/
s/article/{article}/
s/by Charles Dickens/{by Charles Dickens}/
s/\\begindocument/\\begin{document}/
s/\\enddocument/\\end{document}/



