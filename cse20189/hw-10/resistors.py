#!/usr/bin/python

import math
import sys

color_to_dec = {'black': 0, 'brown': 1, 'red': 2, 'orange': 3, 'yellow': 4, 'green': 5, 'blue': 6, 'violet': 7, 'grey': 8, 'white': 9}

def bands_to_number(color1, color2, color3):
	one = color_to_dec[color1]
	two = color_to_dec[color2]
	three = color_to_dec[color3]
	resist = one*two*(10**three)
	return resist

def reverse_dictionary(d):
	dec_to_color = dict((v,k) for k,v in d.items())
	return dec_to_color

def multiplier_value(n):
	return int(math.log10(n/10))

def check_number_input(i):
	div = i/(int(str(1)+str(multiplier_value(i))))
	divstr = str(div)
	if len(divstr)<2:
		return False
	elif divstr[1] == 0:
		return False
	elif multiplier_value(i)>9:
		return False
	else:
		return True

def number_to_bands(n):
	c = check_number_input(n)
	if c==False:
		print str(n)+' is not a valid number'
		sys.exit(1)
	d = reverse_dictionary(color_to_dec)
	string = str(n)
	str1 = d[int(string[0])]
	str2 = d[int(string[1])]
	str3 = d[multiplier_value(n)]
	l = (str1, str2, str3)
	lis = list(l)
	return lis
	
if __name__ == '__main__':
	print 'green is '+str(color_to_dec['green'])
	print 'white is '+str(color_to_dec['white'])
	print 'red is '+str(color_to_dec['red'])
	d = number_to_bands(29000000)
	print '29000000 returns '+d[0]+', '+d[1]+', '+d[2]


	
