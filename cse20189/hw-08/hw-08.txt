Homework 8: Questions 2 and 3

2a.
gcc -g3 -O0 rot13.c -o rot13 

2b.
(gdb) b 59
Since the program doesn't break, then you know the for loop wasn't run once.

2c.
changed for (i=2; i<argc; i++) to for(i=1; i<argc; i++)
see rot13.c

2di. (gdb) b 60

2dii.
after creating a breakpoint after len is assigned (b 33)
(gbd) print len

2diii. after creating breakpoint:
(gdb) commands
>printf "\n%i ", len
>printf "%s", translation
>end

2div.With the current while condition, the loop will try to run len+1 times, which does not work. You would either have to make the condition len>1 or use len-1 in the indexing

2dv.translation[len]=rot13_translate_character(str[len]);
here len should be len-1 because the program is trying to set an index of the string that was not allocated.


2e. valgrind shows a memory leak and invalid read. The memory leak could be fixed by changing the calloc statement in the translate string function.

3a. watch val, run, old value of val is -1 and new value is 859

3b.watch abort_flag, run, backtrace, check counter is in frame #0

3c. p main::j says j=859

