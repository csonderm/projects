#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char rot13_translate_character(char c)
{
	if( 'A' <= c && c <= 'M' )
	{
		return c + 13;
	}
	else if( 'N' <= c && c <= 'Z' )
	{
		return c - 13;
	}
	else if( 'a' <= c && c <= 'm' )
	{
		return c + 13;
	}
	else if( 'n' <= c && c <= 'z' )
	{
		return c - 13;
	}
	else 
	{
		return c;
	}
}

char *rot13_translate_string(const char *str)
{
	int  len          = strlen(str);
	char *translation = calloc(len, sizeof(char));

	int i;

	do
	{
		/* Translate each character, starting from the end of the string. */
		translation[len-1] = rot13_translate_character(str[len-1]);
		len--;
	} while( len > 0 );

	return translation;
}

int main(int argc, char **argv)
{
	if( argc < 1)
	{
		/* If no arguments, print usage message using the command name in argv[0] */
		fprintf(stderr, "Usage: %s word [word ...]\n", argv[0]);
		return 1;
	}

	/* Translate each of the arguments */
	int i;
	for( i = 1; i < argc; i++) //fixed. original (i=2; i<argc)
	{
		char *translation =  rot13_translate_string( argv[i] );
		fprintf(stdout, "%s\n", translation);
	}

	return 0;
}

