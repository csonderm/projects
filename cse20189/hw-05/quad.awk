#quadratic solver
#print out a:b:c:x1:x2
#Use cat coefs.txt | awk -F":" -f quad.awk
#in command line
{x1=quadratic($1, $2, $3, "+")}
{x2=quadratic($1, $2, $3, "-")}
{$1=$1} 
{OFS=":"}
{print $1,$2,$3,x1,x2}


function quadratic(a, b, c, sign) {
	if(sign=="+"){	
		return ((b*(-1))+sqrt((b*b)-(4*a*c)))/(2*a)
	}
	else {
		return ((b*(-1))-sqrt((b*b)-(4*a*c)))/(2*a)
	}
}
