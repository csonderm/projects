#must type command: ps au | awk -f psroot.awk in terminal
/root/ {sum+=$3}

END{
	print "Total summed CPU % for root: ", sum
}
