BEGIN{FS=","}
{{OFS="|"}
{if (NR==1)
{
	gsub("km", "miles")
	print $1, $3
}
else 
{
	print $1,kmtomiles($3)}
}
}
function kmtomiles(k) 
{	
	return (k*0.621371)
}

