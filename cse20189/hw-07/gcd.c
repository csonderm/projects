#include <stdio.h>

int gcd( int a, int b ) {

   if( a == 0 || b == 0 )
      return a + b;

//bug: what happens when a or b are negative?
   if( a < b )
      return gcd(b - a, a);
   else
      return gcd(a - b, b);
}

int main(int argc, char **argv) {
   int a = atoi( argv[1] );
   int b = atoi( argv[2] );

   int q = gcd(a, b);

   fprintf(stdout, "%d\n", q);

   return 0;
}


