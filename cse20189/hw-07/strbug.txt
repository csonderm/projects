a) 2 valgring error reports:
Invalid read of size 1: The program is trying to read memory where it should not in the function print_copy. The program does not properly use malloc in line 11 indicated by the statement in valgrind that the pointer is 0 bytes after a block of size 5 is allocated. Also len should be len+1 in the malloc. The program is trying to read in fprintf out of bounds.

Invalid write of size 1: The program is trying to write memory where it should not. The same errors above apply. Here when using strcpy in print_copy the program has a bug when trying to write since the strings may not be the same length and is looking to write out of bounds. It did not allocate enough memory to copy the input string to the malloced string. len should be len+1 in the malloc statement.

Things were not freed at the end of the of program that were malloced.

b) 
Most likely if you have errors there might be a memory leak. But if you look in the HEAP SUMMARY: you can see that 5 bytes were in use at exit, with 1 alloc and 0 frees. It states that 5 bytes in 1 blocks are definitely lost. In the LEAK SUMMARY towards the bottom of the valgrind report, you will see a report of losses with definitely lost: 5 bytes in 1 blocks. This indicates a memory leak.
