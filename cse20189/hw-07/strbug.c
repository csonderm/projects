#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_copy(const char *input_str)
{
	int        len = strlen(input_str);

// this should be len + 1. why?
	char *copy_str = malloc( len );

	strcpy(copy_str, input_str);

	fprintf(stdout, "Input string: %s\n", input_str);
	fprintf(stdout, "Copy  string: %s\n", copy_str);
}

int main(int argc, char **argv)
{
	if( argc != 2 ) {
		fprintf(stderr, "Usage: %s some-string\n", argv[0]);
		exit(1);
	}

	char *input_str = argv[1];

	print_copy(input_str);

	return 0;
}
