/*
Claire Sonderman
Project 2: myshell
February 18, 2015

This is a program to simulate an environment shell.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/signal.h>
#include <errno.h>

#define BUFSIZE 4096

int main(int argc, char *argv[])
{
	// Check arguments
	if (argc!=1){
		printf("Run myshell by typing: myshell or myshell < fileinput\n");
		exit(1);
	}
	

	// Start prompt
	while(1){
		// Declare variables
		char buffer[BUFSIZE];
		char *temp;
		char *words[100];
		int nwords=0;

		
			// Print prompt
			printf("myshell> ");
			fflush(stdout);
	
			// Read in input line and split string
			if (fgets(buffer, BUFSIZE, stdin)!=NULL) {
				temp = strtok(buffer, " \t\n");
				while (temp!=NULL) {
					words[nwords] = temp;
					nwords++;
					temp = strtok(0, " \t\n");
				}
				words[nwords]=0;
			} else {
				exit(0);
			}
		

		// Check if no command
		if (words[0]==NULL){
			continue;
		}

		// Extract just the command
		int i;
		char *command[100];
		for (i=1; i<=(nwords); i++) {
			command[i-1]=words[i];
		}
		
		// Get command and do action
		if (strcmp(words[0], "start")==0) {
			if (nwords==1){
				printf("myshell: Nothing provided to start\n");
				continue;
			}
			pid_t pid = fork();
			if (pid==0) {
				// This is the child, run execvp
				if(execvp(command[0], command)<0){
					printf("myshell: could not execute command: %s\n", strerror(errno));
					exit(1);
				}
			} else if (pid<0) {
				printf("myshell: could not fork: %s\n", strerror(errno));
				exit(1);
			} else {
				printf("myshell: process %i started\n", pid);
			}
		}

		else if (strcmp(words[0], "wait")==0){
			if (nwords!=1){
				printf("myshell: to wait, simply type 'wait'\n");
				continue;
			}
			pid_t waitstat;
			int status;
			waitstat = wait(&status);
			if(waitstat<0){
				printf("myshell: no processes left\n");
			}
			else{ // We don't need the stop or continue but I put this in for practice
				if (WIFEXITED(status)){
					if(status==0){
						printf("myshell: the process %i exited normally with status %d\n", waitstat, WEXITSTATUS(status), strsignal(status));
					}else{
						printf("myshell: the process %i exited abnormally with signal %d: %s\n", waitstat, WEXITSTATUS(status), strsignal(status));
					}
				} else if (WIFSIGNALED(status)){
					printf("myshell: the process %i exited abnormally with signal %d: %s\n", waitstat, WTERMSIG(status), strsignal(status));
				} else if (WIFSTOPPED(status)){
					printf("myshell: the process %i was stopped with signal %d: %s\n", waitstat, WSTOPSIG(status), strsignal(status));
				} else if (WIFCONTINUED(status)){
					printf("myshell: the process %i was continued: %s\n", waitstat, strsignal(status));
				} else {
					printf("myshell: unknown status for process %i: %s\n", waitstat, strsignal(status));
				}
			}
		}
		else if (strcmp(words[0], "run")==0){
			if (nwords==1){
				printf("myshell: Nothing provided to run\n");
				continue;
			}
			pid_t pid2 = fork();
			int childstatus;
			if (pid2<0){
				printf("myshell: could not fork: %s\n", strerror(errno));
			}
			else if (pid2==0){
				// This is the child, run execvp
				if(execvp(command[0], command)<0){
					printf("myshell: could not execute command: %s\n", strerror(errno));
					exit(1);
				}
			}

			else{
				int waiting = waitpid(pid2, &childstatus, WUNTRACED||WCONTINUED);
				if(waiting<0){
					printf("myshell: could not wait for process: %s\n", strerror(errno));
				}
				else {
					if (WIFEXITED(childstatus)){
						if(childstatus==0){
							printf("myshell: the process %i exited normally with status %d\n", waiting, WEXITSTATUS(childstatus), strsignal(childstatus));
						}else{
							printf("myshell: the process %i exited abnormally with signal %d: %s\n", waiting, WEXITSTATUS(childstatus), strsignal(childstatus));
						}
					} else if (WIFSIGNALED(childstatus)){
						printf("myshell: the process %i exited abnormally with signal %d: %s\n", waiting, WTERMSIG(childstatus), strsignal(childstatus));
					} else if (WIFSTOPPED(childstatus)){
						printf("myshell: the process %i was stopped with signal %d: %s\n", waiting, WSTOPSIG(childstatus), strsignal(childstatus));
					} else if (WIFCONTINUED(childstatus)){
						printf("myshell: the process %i was continued: %s\n", waiting, strsignal(childstatus));
					} else {
						printf("myshell: unknown status for process %i: %s\n", waiting, strsignal(childstatus));
					}
				}	
			}
		}
		else if (strcmp(words[0], "kill")==0) {
			if (nwords!=2){
				printf("myshell: kill syntax: kill processid\n");
				continue;
			}
			int killpid = atoi(words[1]);
			if (kill(killpid, SIGKILL)!=0) {
				printf("myshell: could not kill process %i: %s\n", killpid, strerror(errno));
			} else {
				printf("myshell: process %i killed\n", killpid);
			}
		} else if (strcmp(words[0], "stop")==0) {
			if (nwords!=2){
				printf("myshell: stop syntax: stop processid\n");
				continue;
			}
			int stoppid = atoi(words[1]);
			if (kill(stoppid, SIGSTOP)!=0) {
				printf("myshell: could not stop process %i: %s\n", stoppid, strerror(errno));
			} else {
				printf("myshell: process %i stopped\n", stoppid);
			}
		} else if (strcmp(words[0], "continue")==0) {
			if (nwords!=2){
				printf("myshell: continue syntax: continue processid\n");
				continue;
			}
			int continuepid = atoi(words[1]);
			if (kill(continuepid, SIGCONT)!=0) {
				printf("myshell: could not continue process %i: %s\n", continuepid, strerror(errno));
			} else {
				printf("myshell: process %i continued\n", continuepid);
			}
		} else if ((strcmp(words[0], "quit")==0)||(strcmp(words[0], "exit")==0)) {
			exit(0);
		} else {
			printf("Can not understand command: %s\n", words[0]);
		}
	}

	return 0;
}
