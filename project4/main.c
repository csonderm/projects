/*
Claire Sonderman
Page Fault Handler - Project 4 - Virtual Memory
April 21, 2016

Main program for the virtual memory project
Make all of your modifications to this file.
You may add or rearrange any code or data as you need.
The header files page_table.h and disk.h explain
how to use the page table and disk interfaces.
*/

#include "page_table.h"
#include "disk.h"
#include "program.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <ctype.h>

// Frame Table struct  
typedef struct framet{
	int numFrames;
	int numPages;
	char method;
	int isFull;
	int currFrame; 

	// Each entry in this array is the page that maps to it
	int * framesToPages; 
	int * pagesToFrames;
	int * pagesWritten;

} frameTable;


// Global Variables
frameTable fTable;
struct disk *disk = NULL; 

int diskReads = 0;
int diskWrites = 0;
int pageFaults = 0;

// For fifo
int * queue = NULL;


// Page-replacement algorithms:
#define RANDOM 'r'
#define FIFO 'f'
#define CUSTOM 'c'

// For custom
#define MAX 5


void page_fault_handler( struct page_table *pt, int page ) {
	// Increase page fault total for this program
	pageFaults++;

	// Get physical memory and allocate space, moved this here from main
	char *physmem = page_table_get_physmem(pt);

	int *frame = malloc(sizeof(int));
	int *bits = malloc(sizeof(int));

	// Check to see if we can allocate
	if ((bits == NULL) || (frame==NULL)) {
		printf("couldn't allocate memory!\n");
		exit(1);
	}

	// Update bits if page is in page table
	if (fTable.pagesWritten[page] > 0) {
		page_table_set_entry(pt,page,fTable.pagesToFrames[page],PROT_READ|PROT_WRITE);


	// If table isn't full yet, then fill - don't need replace methods
	} else if (!fTable.isFull) {
		// Set the page table entry 
		page_table_set_entry(pt,page,fTable.currFrame,PROT_READ);

		// Read from disk and add to total diskReads
		disk_read(disk, page, &physmem[fTable.currFrame*PAGE_SIZE]);
		diskReads++;

		// Update frame table 
		fTable.framesToPages[fTable.currFrame] = page;
		fTable.pagesWritten[page] = 1;
		fTable.pagesToFrames[page] = fTable.currFrame;
		queue[fTable.currFrame] = fTable.currFrame;

		// Check if the table is full and set isFull to 1 if so, 
		// otherwise update currFrame
		if(fTable.currFrame >= fTable.numFrames-1)
			fTable.isFull = 1;
		else
			fTable.currFrame++;


	// Page replacement time, could use a switch case 
	} else {
		int repFrame;	// replace
		int p,i;	

		// Random  
		if (fTable.method == RANDOM) {
			// Get a random int and mod with number of frames
			repFrame = lrand48() % fTable.numFrames;

			p = fTable.framesToPages[repFrame];
			page_table_get_entry(pt,p,frame,bits);

			// If there is a dirty bit (3-WRITE), write to disk
			if(*bits == 3){
				disk_write(disk,p,&physmem[(*frame)*PAGE_SIZE]);
				diskWrites++;
			}

			// Read page into frame
			disk_read(disk,page,&physmem[(*frame)*PAGE_SIZE]);
			diskReads++;

			// Set entry in page table
			page_table_set_entry(pt,page,*frame,PROT_READ);
			page_table_set_entry(pt,p,*frame,0);

			// Update frame table
			fTable.framesToPages[*frame] = page;
			fTable.pagesToFrames[page] = *frame;
			fTable.pagesToFrames[p] = -1;
			fTable.pagesWritten[page] = 1;
			fTable.pagesWritten[p] = 0;

		// Fifo
		} else if (fTable.method == FIFO) { 
			// Get first frame in queue
			repFrame = queue[0];

			// Update the queue, move the items by 1
			for(i = 0;i<fTable.numFrames-1;i++){
				queue[i] = queue[i+1];
			} 
			queue[fTable.numFrames-1] = repFrame;

			p = fTable.framesToPages[repFrame];
			page_table_get_entry(pt,p,frame,bits);

			// If there is a dirty bit (3-WRITE), write to disk
			if (*bits == 3) { 
				disk_write(disk,p,&physmem[(*frame)*PAGE_SIZE]);
				diskWrites++;
			}
				
			// Read page into the frame
			disk_read(disk,page,&physmem[(*frame)*PAGE_SIZE]);
			diskReads++;

			// Set entry in page table 
			page_table_set_entry(pt,page,*frame,PROT_READ);
			page_table_set_entry(pt,p,*frame,0);

			// Update frame table
			fTable.framesToPages[*frame] = page;
			fTable.pagesToFrames[page] = *frame;
			fTable.pagesToFrames[p] = -1;
			fTable.pagesWritten[page] = 1;
			fTable.pagesWritten[p] = 0;

		// CUSTOM: Check for dirty bits to decrease extra disk writes
		} else if (fTable.method == CUSTOM) {
			int l = 0;
			int clean = 0;
			
			// Keep getting random frames but check for dirty bits
			// Only do this for a MAX amounts of times and if still
			// no frames without dirty bits, then just pick the 
			// last random one
			while(clean == 0){
				repFrame = lrand48() % fTable.numFrames;

				p = fTable.framesToPages[repFrame];
				page_table_get_entry(pt,p,frame,bits);
			
				if(*bits == 3) {
					clean = 0;
				} else {
					clean = 1;
				}

				l++;
				if (l == MAX) {
					clean = 1;
				}
			}

			// If there is a dirty bit (3-WRITE), write to disk
			if(*bits == 3){
				disk_write(disk,p,&physmem[(*frame)*PAGE_SIZE]);
				diskWrites++;
			}

			// Read page into frame
			disk_read(disk,page,&physmem[(*frame)*PAGE_SIZE]);
			diskReads++;

			// Set entry in page table
			page_table_set_entry(pt,page,*frame,PROT_READ);
			page_table_set_entry(pt,p,*frame,0);

			// Update the frame table
			fTable.framesToPages[*frame] = page;
			fTable.pagesToFrames[page] = *frame;
			fTable.pagesToFrames[p] = -1;
			fTable.pagesWritten[page] = 1;
			fTable.pagesWritten[p] = 0;
		}
	}

	// Free frame and bits
	free(bits);
	free(frame);
	
}

void usage() {
	printf("usage: virtmem <npages> <nframes> <rand|fifo|custom> <sort|scan|focus>\n");
	exit(1);
}

int main(int argc, char *argv[])
{

	// Print usage if there are not exactl 5 arguments
	if(argc!=5) {
		usage();
	}

	// Seed for lrand
	srand48(time(NULL));
	
	// Get pages and frames from command line arguments 
	int npages = atoi(argv[1]);
	int nframes = atoi(argv[2]);
	const char *method = argv[3];
	const char *program = argv[4];


	// User check for correct frames and pages
	if ((npages < 1)||(nframes<1)) {
		printf("Invalid frame or page numbers.\n");
		usage();
	}

	// Initialize the frame table
	fTable.numFrames = nframes;
	fTable.numPages = npages;
	fTable.isFull = 0;
	fTable.currFrame = 0;

	// Allocate memory for the frame table and bits
	fTable.framesToPages = (int*) malloc(sizeof(int)*fTable.numPages);
	memset(fTable.framesToPages,0,sizeof(fTable.framesToPages));

	fTable.pagesWritten = (int*) malloc(sizeof(int)*fTable.numPages);
	memset(fTable.pagesWritten,0,sizeof(fTable.pagesWritten));

	fTable.pagesToFrames = (int*) malloc(sizeof(int)*fTable.numPages);
	memset(fTable.pagesToFrames,-1,sizeof(fTable.pagesToFrames));

	queue = malloc(sizeof(int)*fTable.numFrames);
	memset(queue,-1,sizeof(fTable.numFrames));

	// Get page replacement method and default to random if they type something different
	if(!strcmp(method,"rand")) {
		fTable.method = RANDOM;
	} else if(!strcmp(method,"fifo")) {
		fTable.method = FIFO;
	} else if(!strcmp(method,"custom")) {
		fTable.method = CUSTOM;
	} else {
		fprintf(stderr,"unknown method: %s\n",argv[3]);
		usage();
	}

	// Create Virtual disk
	disk = disk_open("myvirtualdisk",npages);
	if(!disk) {
		fprintf(stderr,"couldn't create virtual disk: %s\n",strerror(errno));
		return 1;
	}

	// Create the page table
	struct page_table *pt = page_table_create( npages, nframes, page_fault_handler );
	
	if(!pt) {
		fprintf(stderr,"couldn't create page table: %s\n",strerror(errno));
		return 1;
	}

	char *virtmem = page_table_get_virtmem(pt);
	
	// Pick scan, sort, or focus
	if(!strcmp(program,"sort")) {
		sort_program(virtmem,npages*PAGE_SIZE);
	} else if(!strcmp(program,"scan")) {
		scan_program(virtmem,npages*PAGE_SIZE);
	} else if(!strcmp(program,"focus")) {
		focus_program(virtmem,npages*PAGE_SIZE);
	} else {
		fprintf(stderr,"unknown program: %s\n",argv[4]);
		return 1;
	}

	// Print results
	printf("Page Faults: %d\n", pageFaults);
	printf("Disk Reads: %d\n", diskReads);
	printf("Disk Writes: %d\n", diskWrites); 

	// Delete page table and close the disk
	page_table_delete(pt);
	disk_close(disk);

	// Free up our variables
	free(fTable.framesToPages);
	free(fTable.pagesWritten);
	free(fTable.pagesToFrames);

	return 0;
}
