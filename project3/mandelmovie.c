/*
Claire Sonderman
Project 3, mandelmovie.c
March 24, 2016
*/

#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/signal.h>


int main( int argc, char *argv[] ) {

	int i=1;
	double a = 2;
	double b = 0.000005;
	double mult = exp(log(b/a)/49);		// Smooths out the 
	int n=1;
	char *ptr;
	int running = 0;			// Total running processes at a time

	if (argc==2) {
		n = strtol(argv[1], &ptr, 10);
	} else if (argc > 2) {
		printf("Too many arguments! Use form: ./mandel or ./mandel <int>\n");
		exit(1);
	} 	

	// Do 50 calls to mandel. 
	// If running processes total reaches n, 
	// wait and then decress running count
	while (i<51) {
		if (running<n) {	
			if (i>1) {
				a=a*mult;
			}
      			pid_t pid = fork();
			if (pid==0) {
				char mandel[70];
				sprintf(mandel, "./mandel -x 0.286932 -y 0.014287 -s %lf -m 500 -W 1024 -H 1024 -o mandel%d.bmp", a, i);
				
				// run mandel
				char *temp;
				int nwords = 0;
				char *words[100];
				temp = strtok(mandel, " \t\n");
				while (temp!=NULL) {
					words[nwords] = temp;
					nwords++;
					temp = strtok(0, " \t\n");
				}
				words[nwords]=0;
			  	if (execvp(words[0], words)<0) {
					printf("Could not execute command mandel: %s\n", strerror(errno));
					exit(1);
				} 
				
			} else if (pid<0) {
				printf("Could not fork: %s\n", strerror(errno));
				exit(1);
			} else {
				i++;
				running++;
			}
		} else {	
			// Wait if too many running processes
			pid_t waitstat;
			int status;
			waitstat = wait(&status);
			if (waitstat<0) {
				printf("No more processes to wait for.\n");
			} else { 
				if (WIFEXITED(status)) {
					if(status==0){
						running--;
					} else {
						printf("mandel%d.bmp process exited abnormally.\n", i);
					}
				} else {
					printf("mandel%d.bmp process has error.\n", i);
				}
			}
		}		
	}

	
	// Wait for the still running processes, could also integrate with loop above
	while (running>0) {
		pid_t w = 0;
		int stat;
		w = wait(&stat);
		if (w<0) {
			return 0;
		} else {
			if (WIFEXITED(stat)) {
				if (stat==0) {
					running--;
				} else {
					printf("mandel%d.bmp process exited abnormally.\n", i);
				}
			} else {
				printf("mandel%d.bmp process has error.\n", i);
			}
		}	
	}

	return 0;
}

